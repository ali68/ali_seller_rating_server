const restify = require('restify');
const restifyPromise = require('restify-await-promise');
const redis = require('redis');
const {promisify} = require('util');
const corsMiddleware = require('restify-cors-middleware');
const Logger = require('bunyan');
const log = new Logger.createLogger({
    name: 'aliexpress_seller_rating',
    serializers: {
        req: Logger.stdSerializers.req
    }
});
let port = 5000;
let redisConnection = {
    host: 'redis-17042.c15.us-east-1-2.ec2.cloud.redislabs.com',
    port: 17042,
    password: 'xEGvuxrE7FHc9gnVq4DCMqT5OX1lGHbf',
}


const _ = require('lodash');

const server = restify.createServer({
    name: 'aliexpress_seller_rating',
    version: '0.0.1',
    log: log
});

server.pre(function (request, response, next) {
    request.log.info({ req: request }, 'REQUEST');
    next();
});

if(process.env.NODE_ENV === 'production'){
    port = 80;
    // redisConnection = {
    //     host: '127.0.0.1',
    //     port: 6379,
    //     password: 'b994e831bb67e8928a6b9fe5d3cbd24aae79dfb36754982ef169edbc9535e41c'
    // }
    let corsConfig = {
        preflightMaxAge: 5, //Optional
        origins: ['chrome-extension://odbpnjodjjodaiboakmaepncbccnkmgb', 'chrome-extension://cbopiafampdaigdcflmjoialcenedall'],
        credentials: true
    }

    const cors = corsMiddleware(corsConfig);
    server.pre(cors.preflight);
    server.use(cors.actual);
}

server.use(restify.plugins.acceptParser(server.acceptable));
// server.use(restify.plugins.authorizationParser());
server.use(restify.plugins.dateParser());
server.use(restify.plugins.queryParser());
server.use(restify.plugins.jsonp());
server.use(restify.plugins.gzipResponse());
server.use(restify.plugins.bodyParser());

const redisClient = redis.createClient(redisConnection);
const getAsync = promisify(redisClient.get).bind(redisClient);
const setAsync = promisify(redisClient.set).bind(redisClient);

//Allows you to manipulate the errors before restify does its work
const alwaysBlameTheUserErrorTransformer = {
    transform: function (exceptionThrownByRoute) {
        //Always blame the user
        exceptionThrownByRoute.statusCode = 400;
        return exceptionThrownByRoute;
    }
}

const options = {
    errorTransformer: alwaysBlameTheUserErrorTransformer //Optional: Lets you add status codes
};

restifyPromise.install(server, options);

// server.get('/', function (req, res, next) {
//     res.send('version 0.0.1');
//     next();
// });

server.get('/', function (req, res, next) {
    res.redirect(302, 'https://goo.gl/qQ8Mu6', next);
});

server.post('/store/', async (req, res, next) => {
    let store = JSON.parse(req.body);
    if(store.storeNum){
        const ttl = 2592000; //1 month
        let expiredAt = new Date();
        expiredAt = expiredAt.setMonth(expiredAt.getMonth() + 1);
        store = _.assignIn(store, {expiredAt: expiredAt});
        const rs = await setAsync(`store_${store.storeNum}`, JSON.stringify(store), 'EX', ttl);
        return res.send(rs);
    }
    return res.send(null);
});

server.get('/store/:id', async (req, res, next) => {
    const storeNum = req.params.id;
    const storeKey = `store_${storeNum}`;
    let rs = await getAsync(storeKey);
    if(rs){
        res.json(JSON.parse(rs)).end();
    }else{
        return null;
    }
})


server.listen(port, function () {
    console.log('%s listening at %s', server.name, server.url);
})

